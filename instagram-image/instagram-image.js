angular.module('tagflow.instagramImage', [])
.directive('instagramImage', ['$window', function ($window) {
	'use strict';
	return {
		restrict: 'E',
		templateUrl: 'components/instagram-image/instagram-image.html',
		scope: {
			photo: '=source'
		},
		controller: ['$scope', function ($scope) {
			$scope.openImage = function (photoLink) {
				$window.open(photoLink);
			};
			$scope.openUserProfile = function (username) {
				var url = 'https://instagram.com/' + username;
				$window.open(url); 
			};
		}]
	};
}]);