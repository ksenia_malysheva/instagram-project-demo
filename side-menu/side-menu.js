angular.module('tagflow.sideMenu', ['tagflow.instagramApi', 
	'tagflow.userConfig', 
	'tagflow.userSubscriptions', 
	'customUsersSubscriptions'
])
.directive('sideMenu', [
	'instagramOAuth', 
	'userConfig', 
	'userSubscriptions', 
	'customTopics', 
	function (instagramOAuth, userConfig, userSubscriptions, customTopics) {

	'use strict';
	return {
		restrict: 'E',
		templateUrl: 'components/side-menu/side-menu.html',
		scope: {},
		controller: ['$scope', function ($scope) {
			$scope.topics = customTopics.getTopics();
			
			$scope.addTopic = function (topic) {
				var i, length = topic.users.length;
				userSubscriptions.clearSubscriptions();
				for (i = 0; i < length; i++) {
					userSubscriptions.addParticularUserSubscription(topic.users[i].name, topic.users[i].instagramId);
				} 

			};
			$scope.logIn = function () {
				instagramOAuth.redirectToInstagramAuth();
			};
			$scope.logOut = function () {
				userConfig.accessToken = null;
				$scope.loggedIn = false;
			};
			$scope.loggedIn = userConfig.accessToken ? true : false;
		}]
	};
}]);